package com.gutierrez.daniel.challenge.test;


import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.gutierrez.daniel.challenge.model.Cart;
import com.gutierrez.daniel.challenge.model.Item;
import com.gutierrez.daniel.challenge.model.Purchase;
import com.gutierrez.daniel.challenge.service.CartService;
import com.gutierrez.daniel.exception.DAOException;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
/**
 * Class for unit tests
 * @author daniel
 *
 */
public class ChallengeApplicationTests {

	@Autowired
	private MockMvc mvc;
	
	
	@MockBean
	private CartService cartService;
		
	/**
	 * Method responding ok is validated
	 * @throws Exception
	 */
	@Test
	public void createCartTest() throws Exception{
		when(cartService.saveNewCart()).thenReturn(new Cart(1));
		mvc.perform(post("/cart")).andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
	}
	
	/**
	 * Content is validate on create cart method
	 * @throws Exception
	 */
	@Test
	public void createCartTest_Content() throws Exception{
		Cart cart = new Cart(1);
		when(cartService.saveNewCart()).thenReturn(cart);
		mvc.perform(post("/cart")).andExpect(status().isOk()).andExpect(jsonPath("$.id", is(cart.getId())));
		
	}
	
	/**
	 * Correct error shown when system is not allowing
	 * I/O Operation for the persistence operations
	 * @throws Exception
	 */
	@Test
	public void createCartTest_FileSystemError() throws Exception{
		when(cartService.saveNewCart()).thenThrow(IOException.class);
		mvc.perform(post("/cart")).andExpect(status().is(HttpStatus.INTERNAL_SERVER_ERROR.value()));
	}
	
	/**
	 * Basic getCart operation tested
	 * @throws Exception
	 */
	@Test
	public void getCart() throws Exception{
		Cart cart = builCart(10);
		when(cartService.findById(cart.getId())).thenReturn(cart);
		mvc.perform(get("/cart/10")).andExpect(status().isOk()).andExpect(jsonPath("$.id", is(cart.getId())));
	}
	
	/**
	 * Validate empty response and not found status 
	 * when cart does not exist
	 * @throws Exception
	 */
	@Test
	public void getCartTest_NotFound() throws Exception{
		Cart cart = builCart(12);
		when(cartService.findById(cart.getId())).thenReturn(null);
		mvc.perform(get("/cart/10")).andExpect(status().isNotFound()).andExpect(content().string(""));
	}
	/**
	 * Add quantity tested with 1 value
	 * @throws Exception
	 */
	@Test
	public void addQuantityTest_addOneQnty() throws Exception{
		Cart cart = builCart(1);
		Purchase purchase = cart.getPurchases().get(0);
		int quantity = 1;
		when(cartService.addQuantityToPurchase(cart,cart.getPurchases().get(0),quantity)).thenReturn(purchase.getQuantity()+quantity);
		mvc.perform(put("/cart/1/1/changeQuantity").param("amount", String.valueOf(quantity))).andExpect(status().isOk()).andExpect(content().string(""));
	}
	/**
	 * Add quantity tested with 10 value
	 * @throws Exception
	 */
	@Test
	public void addQuantityTest_addTenQnty() throws Exception{
		Cart cart = builCart(1);
		Purchase purchase = cart.getPurchases().get(0);
		int quantity = 10;
		when(cartService.addQuantityToPurchase(cart,cart.getPurchases().get(0),quantity)).thenReturn(purchase.getQuantity()+quantity);
		mvc.perform(put("/cart/1/1/changeQuantity").param("amount", String.valueOf(quantity))).andExpect(status().isOk()).andExpect(content().string(""));
	}
	
	/**
	 * Add purchase tested
	 * @throws Exception
	 */
	@Test
	public void addPurchaseToCartTest() throws Exception{
		Cart cart = builCart(1);
		Purchase purchase = cart.getPurchases().get(0);
		when(cartService.addPurchaseToCart(purchase,cart)).thenReturn(cart);
		mvc.perform(put("/cart/1").content("{\"id\" : 1, \"title\" :\"Product A\",\"price\" : 18.50 }").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}
	
	/**
	 * addPurchase to a non-existing cart
	 * @throws Exception
	 */
	@Test
	public void addPurchaseToCartTest_NonExistingCart() throws Exception{
		Cart cart = builCart(99);
		Purchase purchase = cart.getPurchases().get(0);
		 when(cartService.addPurchaseToCart(purchase,cart)).thenThrow(DAOException.class);
		 mvc.perform(put("/cart/{id}",cart.getId()).contentType(MediaType.APPLICATION_JSON).content("{\"id\" : 99, \"title\" :\"Product A\",\"price\" : 18.50 }")).andExpect(status().isNotFound());
		
	}

	
	/**
	 * Basic mock of cart object
	 * @param code
	 * @return
	 */
	private Cart builCart(int code) {
		Cart cart = new Cart(code);
		cart.addPurchase(new Item(99, "A", 20.50));
		cart.addPurchase(new Item(2, "B", 40.00));
		return cart;
	}
	
	

}
