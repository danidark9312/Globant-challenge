package com.gutierrez.daniel.challenge.service;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gutierrez.daniel.challenge.model.Cart;
import com.gutierrez.daniel.challenge.model.Purchase;
import com.gutierrez.daniel.challenge.repository.CartRepository;
import com.gutierrez.daniel.exception.BusinesException;
import com.gutierrez.daniel.exception.DAOException;


@Service
public class CartServiceImpl implements CartService{

	@Autowired
	CartRepository cartRepository;
	
	@Override
	public Cart findById(int id) throws DAOException, IOException{
		return cartRepository.findById(id);
	}

	@Override
	public Cart addPurchaseToCart(Purchase purchase, Cart cart)throws DAOException, IOException {
		System.out.println("----------------- addpurchase -------------");
		Cart cartFound = cartRepository.findById(cart.getId());
		int purchaseIndex = -1;
		
		if(cartFound==null)
			throw new DAOException("Cart Not Found");
		
		if(cartFound.getPurchases() != null && cartFound.getPurchases().size()>0)
			purchaseIndex = cartFound.getPurchases().indexOf(purchase);
		
		if(purchaseIndex==-1) {
			cartFound.addPurchase(purchase);
		}else {
			cartFound.getPurchases().get(purchaseIndex).increaseQuantity();	
		}
		
		return cartRepository.merge(cartFound);
	}
	@Override
	public boolean removePurchaseFromCart(Purchase purchase, Cart cart)throws DAOException, IOException {
		Cart cartFound = cartRepository.findById(cart.getId());
		boolean isRemoved = cartFound.removePurchase(purchase);
		cartRepository.merge(cartFound);
		return isRemoved;
	}


	@Override
	public Cart saveNewCart()throws DAOException, IOException {
		return cartRepository.saveNewCart();
	}

	@Override
	public int addQuantityToPurchase(Cart cart, Purchase purchase, int amount)throws DAOException, IOException, BusinesException {
		Cart cartFound = cartRepository.findById(cart.getId());
		int purchaseIndex = -1;
		if(cartFound!=null && cartFound.getPurchases() != null && cartFound.getPurchases().size()>0)
			purchaseIndex = cartFound.getPurchases().indexOf(purchase);
		
		if(purchaseIndex==-1)
			return -1;
		
		Purchase purchaseFound = cartFound.getPurchases().get(purchaseIndex);
		
		if(amount < 0 && purchaseFound.getQuantity()+amount<0)
			throw new BusinesException("Resulting Quantity can not be less than zero");
		
		purchaseFound.addQuantity(amount);
		
		//If the resulting quantity is zero, the article should be removed from the cart
		if(purchaseFound.getQuantity() == 0) 
			cartFound.removePurchase(purchaseFound);
		
		cartRepository.merge(cartFound);
		return purchase.getQuantity();
	}
	
	
}
