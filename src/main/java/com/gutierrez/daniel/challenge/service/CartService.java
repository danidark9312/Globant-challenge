package com.gutierrez.daniel.challenge.service;

import java.io.IOException;

import com.gutierrez.daniel.challenge.model.Cart;
import com.gutierrez.daniel.challenge.model.Purchase;
import com.gutierrez.daniel.exception.BusinesException;
import com.gutierrez.daniel.exception.DAOException;

public interface CartService {
	/**
	 * 
	 * @param id code that belongs to a existing Cart
	 * @return the cart found, null if no cart was found
	 * @throws DAOException
	 * @throws IOException in case an error ocurre during the read/write of the repository
	 */
	Cart findById(int id) throws DAOException, IOException;
	/**
	 * If the item to purchase is already in the cart
	 * no item will be added, the quantity of the item will 
	 * be increase by 1. if the item is not contained in the cart
	 * a new item will be added
	 * 
	 * @param purchase to be added to the cart
	 * @param cart to be modified
	 * @return true if the operation was successfully excecuted, false otherwise.
	 * @throws DAOException In  case the cart does not exist in the repository
	 * @throws IOException in case an error ocurre during the read/write of the repository
	 */
	Cart addPurchaseToCart(Purchase purchase,Cart cart) throws DAOException, IOException;
	/**
	 * A new cart will be created and saved, and the generated code 
	 * will be returned
	 * @return The new cart with the generated code
	 * @throws DAOException
	 * @throws IOException
	 */
	Cart saveNewCart() throws DAOException, IOException;
	/**
	 * 
	 * @param cart to be modified
	 * @param purchase to be modified
	 * @param qnty Quantity to be added, could be negative for substracting
	 * @return the new Quantity, zero and non-negative number could be retrieve
	 * @throws BusinesException In case the amount is negative and the resulting quantity would be less than zero, 
	 * operation is canceled and exception is thrown.
	 * @throws IOException
	 * @throws DAOException 
	 */
	int addQuantityToPurchase(Cart cart,Purchase purchase, int qnty) throws DAOException, IOException, BusinesException ;
	
	/**
	 * remove one item from the cart
	 * @param purchase to be removed from the cart
	 * @param cart to be modified
	 * @return true if the cart exists and if the purchase belongs to the 
	 * cart and if the removal was successful, otherwise false is returned
	 * @throws DAOException
	 * @throws IOException
	 * 
	 */
	boolean removePurchaseFromCart(Purchase purchase, Cart cart) throws DAOException, IOException;
	
}
