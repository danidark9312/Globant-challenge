package com.gutierrez.daniel.challenge.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileUtil {
	private static Path cachedWorkingPath = null;
	
	/**
	 * 
	 * @param route String representing the folder base 
	 * @return Path representing the current working set for the app
	 * @throws IOException In case the app can not read/write in the file-system
	 */
	public static Path getBaseWorkingPath(String route) throws IOException {
		if(cachedWorkingPath != null)
			return cachedWorkingPath;
		if(route.equals(".")) {
			cachedWorkingPath = Paths.get(System.getProperty("user.dir"),"repository");
		}else {
			cachedWorkingPath = Paths.get(route,"repository");
		}
		if(!Files.exists(cachedWorkingPath)) {
				Files.createDirectories(cachedWorkingPath);
		}
			return cachedWorkingPath;
	}

	/**
	 * 
	 * @param listFiles all files from the working set to extract the next consecutive available 
	 * @return Max consecutive available to be used
	 * to create a new Cart
	 */
	public static int getConsecutive(File[] listFiles) {
		int maxConsecutive = 0;
		for (File file : listFiles) {
			String name = file.getName();
			String strConsecutive= name.substring(name.indexOf("-")+1, name.indexOf("."));
			int consecutive = Integer.parseInt(strConsecutive);
			if(consecutive>maxConsecutive)
				maxConsecutive = consecutive;
		}
		return maxConsecutive+1;
	}
	
	

}
