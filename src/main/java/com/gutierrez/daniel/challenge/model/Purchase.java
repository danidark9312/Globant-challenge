package com.gutierrez.daniel.challenge.model;

public class Purchase {
	private int quantity;
	private Item item;
	
	
	public Purchase(int id, String title, double price, int quantity) {
		this.item = new Item(id, title, price);
		this.quantity = quantity;
	}
	
	public Purchase(Item item) {
		super();
		this.item = item;
		this.quantity = 1;
	}
	public Purchase(Item item, int qnty) {
		super();
		this.item = item;
		this.quantity = qnty;
	}
	public Purchase(Integer itemId) {
		this.item = new Item(itemId, null, 0.0);
	}

	public Item getItem() {
		return item;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setItem(Item item) {
		this.item = item;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public void increaseQuantity() {
		this.quantity++;
	}
	public void decreaseQuantity() {
		this.quantity--;
	}
	public void addQuantity(int amount) {
		this.quantity += amount;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((item == null) ? 0 : item.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Purchase other = (Purchase) obj;
		if (item == null) {
			if (other.item != null)
				return false;
		} else if (!item.equals(other.item))
			return false;
		return true;
	}
	
	
	
}
