package com.gutierrez.daniel.challenge.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
	
public class Cart {
	
	private Integer id;
	private List<Purchase> purchases;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Cart(Integer id) {
		super();
		this.id = id;
	}

	public List<Purchase> getPurchases() {
		return purchases;
	}

	public void setPurchases(List<Purchase> purchase) {
		this.purchases = purchase;
	}
	
	public void addPurchase(Item item) {
		this.addPurchase(item, 1);
	}
	
	public void addPurchase(Item item, int quantity) {
		if(this.purchases == null || this.purchases.size() == 0)
			this.purchases = new ArrayList<>();
		this.purchases.add(new Purchase(item,quantity));
	}
	public void addPurchase(Purchase purchase) {
		if(this.purchases == null || this.purchases.size() == 0)
			this.purchases = new ArrayList<>();
		this.purchases.add(purchase);
	}
	public boolean removePurchase(Purchase purchase) {
		if(this.purchases == null || this.purchases.size() == 0)
			return false;
		return this.purchases.remove(purchase);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cart other = (Cart) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
	
	
}
