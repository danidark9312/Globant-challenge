package com.gutierrez.daniel.challenge.model;

/*
 * Aunque el third party web service retorna todos los items como String
 * Pienso que la forma mas adecuada es manejar el id como entero y el precio
 * como un doble
 * */
public class Item {
	private int id;
	private String title;
	private double price;
	public Item() {
		super();
	}
	public Item(int id, String title, double price) {
		this.id = id;
		this.title = title;
		this.price = price;
	}
	public int getId() {
		return id;
	}
	public double getPrice() {
		return price;
	}
	public String getTitle() {
		return title;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public void setPrice(double price) {
		this.price = price;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Item other = (Item) obj;
		if (id != other.id)
			return false;
		return true;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}	

}
