package com.gutierrez.daniel.challenge.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gutierrez.daniel.challenge.model.Cart;
import com.gutierrez.daniel.challenge.model.Item;
import com.gutierrez.daniel.challenge.model.Purchase;
import com.gutierrez.daniel.challenge.service.CartService;
import com.gutierrez.daniel.exception.BusinesException;
import com.gutierrez.daniel.exception.DAOException;

@RestController
@RequestMapping("cart")
public class CartController {
	@Autowired
	CartService cartService;

	
	@RequestMapping(method = RequestMethod.POST)
    public HttpEntity<Cart> createCart() {
    	HttpEntity<Cart> entity = null;
    	try {
			Cart cart = cartService.saveNewCart();
			entity = new ResponseEntity<>(cart,HttpStatus.OK);
		} catch (DAOException e) {
			e.printStackTrace();
			entity = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (IOException e) {
			e.printStackTrace();
			entity = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
    	return entity;
    }
	
	    @RequestMapping(value = "{cartId}", method = RequestMethod.GET)
	    public HttpEntity<Cart> getCartInfo(@PathVariable Integer cartId) {
	    	Cart cart = null;
	    	HttpEntity<Cart> entity = null;
			try {
				cart = cartService.findById(cartId);
				if(cart != null)
					entity = new ResponseEntity<>(cart,HttpStatus.OK);
				else
					entity = new ResponseEntity<>(cart,HttpStatus.NOT_FOUND);
			} catch (DAOException e) {
				e.printStackTrace();
				entity = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			} catch (IOException e) {
				e.printStackTrace();
				entity = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
	    	return entity;
	    }
	    @RequestMapping(value = "{cartId}/total", method = RequestMethod.GET)
	    public HttpEntity<Double> getInfoToPay(@PathVariable Integer cartId) {
	    	Cart cart = null;
	    	double total = 0.00;
	    	HttpEntity<Double> entity = null;
			try {
				cart = cartService.findById(cartId);
				if(cart != null) {
					total = cart.getPurchases().parallelStream().mapToDouble(p -> p.getItem().getPrice()*p.getQuantity()).sum();
					entity = new ResponseEntity<>(total,HttpStatus.OK);
				}
				else {
					entity = new ResponseEntity<>(HttpStatus.NOT_FOUND);
				}
			} catch (DAOException e) {
				e.printStackTrace();
				entity = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			} catch (IOException e) {
				e.printStackTrace();
				entity = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
	    	return entity;
	    
	    }
	    @RequestMapping(value = "{cartId}/{itemId}", method = RequestMethod.DELETE)
	    public HttpEntity<Void> removePurchase(@PathVariable Integer cartId,@PathVariable Integer itemId) {
	    	Purchase purchase = new Purchase(itemId);
	    	Cart cart = new Cart(cartId);
	    	HttpEntity<Void> entity = null;
		try {
			if (cartService.removePurchaseFromCart(purchase, cart))
				entity = new ResponseEntity<>(null, HttpStatus.OK);
			else
				entity = new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

		} catch (DAOException e) {
	    		e.printStackTrace();
	    		entity = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	    	} catch (IOException e) {
	    		e.printStackTrace();
	    		entity = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	    	}
	    	return entity;
	    	
	    }
	    @RequestMapping(value = "{cartId}/{itemId}/changeQuantity", method = RequestMethod.PUT)
	    public HttpEntity<Void> addQuantity(@PathVariable Integer cartId,@PathVariable Integer itemId,@RequestParam(value = "amount", required = true) Integer amount) {
	    	Purchase purchase = new Purchase(itemId);
	    	Cart cart = new Cart(cartId);
	    	HttpEntity<Void> entity = null;
	    	try {
	    		int finalQuantity = cartService.addQuantityToPurchase(cart,purchase, amount);
	    		if (finalQuantity!=-1)
	    			entity = new ResponseEntity<>(null, HttpStatus.OK);
	    		else
	    			entity = new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
	    		
	    	} catch (DAOException e) {
	    		e.printStackTrace();
	    		entity = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	    	} catch (IOException e) {
	    		e.printStackTrace();
	    		entity = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	    	} catch (BusinesException e) {
	    		e.printStackTrace();
	    		entity = new ResponseEntity<>(HttpStatus.FORBIDDEN);
				e.printStackTrace();
			}
	    	return entity;
	    	
	    }
	    
	    @RequestMapping(value = "{cartId}", method = RequestMethod.PUT)
	    public HttpEntity<Void> addItem(@RequestBody Item item,@PathVariable int cartId) {
	    	Cart cart = new Cart(cartId);
	    	Purchase purchase = new Purchase(item);
	    	HttpEntity<Void> entity = null;
	    	try {
				cartService.addPurchaseToCart(purchase, cart);
				entity = new ResponseEntity<>(HttpStatus.OK);
			} catch (DAOException e) {
				entity = new ResponseEntity<>(HttpStatus.NOT_FOUND);
				e.printStackTrace();
			} catch (IOException e) {
				entity = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
				e.printStackTrace();
			}
	    	return entity;
	    }
	    
}
