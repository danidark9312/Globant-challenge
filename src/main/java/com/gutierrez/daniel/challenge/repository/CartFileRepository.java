package com.gutierrez.daniel.challenge.repository;

import static java.nio.file.StandardOpenOption.READ;
import static java.nio.file.StandardOpenOption.TRUNCATE_EXISTING;
import static java.nio.file.StandardOpenOption.WRITE;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import com.gutierrez.daniel.challenge.model.Cart;
import com.gutierrez.daniel.challenge.model.Item;
import com.gutierrez.daniel.challenge.model.Purchase;
import com.gutierrez.daniel.challenge.util.FileUtil;
import com.gutierrez.daniel.exception.DAOException;


@Repository
public class CartFileRepository implements CartRepository{
	
	@Value( "${fileSystem.workingSet}" )
	private String basePath;
	
	public Cart findById(int id) throws DAOException, IOException{
		Path baseWorkingPath = FileUtil.getBaseWorkingPath(basePath);
		String cartFile = String.format("Cart-%d.dat", id);
		Path finalDir = baseWorkingPath.resolve(cartFile);
		
		if (!Files.exists(finalDir))
			return null;
		
		Cart cart = new Cart(id);
		try(DataInputStream dos = new DataInputStream(Files.newInputStream(baseWorkingPath.resolve(cartFile), READ))){
			while(dos.available()>0) {
				int qnty = dos.readInt();
				int idItem = dos.readInt();
				String title = dos.readUTF();
				double price = dos.readDouble();
				Item item = new Item(idItem, title, price);
				cart.addPurchase(item,qnty);
			}	
		}
		return cart;
	}

	@Override
	public Cart saveNewCart() throws DAOException, IOException {
		Path baseWorkingPath = FileUtil.getBaseWorkingPath(basePath);
		
		Path newCartPath = null;
		Cart cart = null;
		File[] listFiles = baseWorkingPath.toFile().listFiles();
		if(listFiles==null || listFiles.length == 0) {
			newCartPath = baseWorkingPath.resolve("Cart-1.dat");
			cart = new Cart(1);
		}else {
			int consecutive = FileUtil.getConsecutive(listFiles);
			newCartPath = baseWorkingPath.resolve(String.format("Cart-%d.dat", consecutive));
			cart = new Cart(consecutive);
		}
		
		boolean isCreated = newCartPath.toFile().createNewFile();
		System.out.println(newCartPath.toFile());
		if(!isCreated)
			throw new IOException(String.format("Cart could no be created, check the route and permissions %s", newCartPath.toString()));
		
		return cart;
	}
	
	@Override
	public Cart merge(Cart cart) throws DAOException, IOException {
		Path baseWorkingPath = FileUtil.getBaseWorkingPath(basePath);
		String cartFile = String.format("Cart-%d.dat", cart.getId());
		try(
		DataOutputStream raf = new DataOutputStream(Files.newOutputStream(baseWorkingPath.resolve(cartFile), WRITE, TRUNCATE_EXISTING))){
			for (Purchase purchase : cart.getPurchases()) {
				Item item = purchase.getItem();
				raf.writeInt(purchase.getQuantity()); 
				raf.writeInt(item.getId());
				raf.writeUTF(item.getTitle());
				raf.writeDouble(item.getPrice());
			}	
		}
		
		return cart;
	}
	
	
	
}
