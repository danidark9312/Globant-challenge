package com.gutierrez.daniel.challenge.repository;

import java.io.IOException;

import com.gutierrez.daniel.challenge.model.Cart;
import com.gutierrez.daniel.exception.DAOException;

public interface CartRepository {
	
	/**
	 * 
	 * @param id Cart to be found
	 * @return Cart if exists, null otherwise
	 * @throws DAOException if any error ocurres during the searching
	 * @throws IOException
	 */
	
	Cart findById(int id) throws DAOException, IOException;
	
	/**
	 * Creates a new cart in the storage system
	 * @return new cart created
	 * @throws DAOException if an error ocurred trying to save
	 * @throws IOException if the workingset folder does not exist or 
	 * the system does not have the rights to read or write
	 */
	Cart saveNewCart() throws DAOException, IOException;


	/**
	 * Merge cart to the file-system
	 * @param cart to be merged with the underlaid storage
	 * @return cart with all the information merged
	 * @throws DAOException
	 * @throws IOException
	 */
	Cart merge(Cart cart) throws DAOException, IOException;

}
