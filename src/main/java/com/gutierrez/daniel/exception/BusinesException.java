package com.gutierrez.daniel.exception;

public class BusinesException extends Exception{
	private static final long serialVersionUID = 1L;

	public BusinesException() {
		super();
	}

	public BusinesException(String message, Throwable cause) {
		super(message, cause);
	}

	public BusinesException(String message) {
		super(message);
	}
	

}
